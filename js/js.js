
  const app = document.getElementById("app");
  const heightInput = document.getElementById("heightInput");
  const widthInput = document.getElementById("widthInput");
  const rightAngleInput = document.getElementById("rightAngleInput");
  const rotateZ = document.getElementById("rotateZ");

const canvas = document.getElementById("canvas");
const ctx = canvas.getContext("2d");
const pi = Math.PI;

ctx.strokeStyle = "black"
ctx.lineWidth = "3"


let [dot_A_x, dot_A_y] = [100, 200] // dot A
let [dot_B_x, dot_B_y] = [550, 200] // dot B
let [dot_C1_x, dot_C1_y] = [550, 450] // dot C1
let [dot_C2_x, dot_C2_y] = [450, 550] // dot C2
let [dot_D_x, dot_D_y] = [100, 550] // dot D

let figure = function () {
  ctx.arc(450, 450, 100, 0, pi / 2, false);

  ctx.moveTo(dot_C2_x, dot_C2_y)
  ctx.lineTo(dot_D_x, dot_D_y) // down line

  ctx.moveTo(dot_B_x, dot_B_y)
  ctx.lineTo(dot_C1_x, dot_C1_y)  // right line 

  ctx.moveTo(dot_B_x, dot_B_y)
  ctx.lineTo(dot_A_x, dot_A_y)  // top line

  ctx.moveTo(dot_A_x, dot_A_y)
  ctx.lineTo(dot_D_x, dot_D_y)  //  left line

  ctx.clearRect(0, 0, 700, 700)
  ctx.stroke();
}

figure()

// ctx.clearRect(0, 0, 400, 2200)
// ctx.stroke();

heightInput.oninput = function() {
  console.log("height = " + heightInput.value)
};
widthInput.oninput = function() {
  console.log("width = " + widthInput.value)
};
rightAngleInput.oninput = function() {
  console.log("rightAngle = R" + rightAngleInput.value)

};
  
